(defun org-babel-execute:nushell (body params)
  "Evaluate a Nushell script"
  (let* ((nu-script-path (org-babel-temp-file "nushell-babel-script-" ".nu"))
         (nu-config-path (org-babel-temp-file "nushell-babel-config-" ".nu"))
         (nu-env-path (org-babel-temp-file "nushell-babel-env-" ".nu"))
         (nu-command (string-trim (format "nu --config %s --env-config %s %s"
                                          nu-config-path
                                          nu-env-path
                                          nu-script-path)))
         (nu-config (string-join '("$env.config.use_ansi_coloring = false"
                                   "$env.config.table.mode = markdown") "\n")))

    ;; Write config to the config file
    (with-temp-file nu-config-path (insert nu-config))

    ;; Build up an env file
    ;; For each var in header params:
    ;; - create a temporary file,
    ;; - write the value into the file
    ;; - append line "let <name> = open <path>" to the env file
    (with-temp-file nu-env-path
      (cl-loop for (name . value) in (org-babel--get-vars params)
               do (let* ((var-file-path (org-babel-temp-file "nushell-babel-var-")))
                    (with-temp-file var-file-path (insert value))
                    (insert (format "let %s = open %s\n" name var-file-path)))))

    ;; Write the script itself
    (with-temp-file nu-script-path (insert body))


    ;; Drumroll...
    (org-babel-eval nu-command "")))
